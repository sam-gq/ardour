Source: ardour
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Adrian Knoth <adi@drcomp.erfurt.thur.de>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
 Dennis Braun <d_braun@kabelmail.de>
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 gettext,
 intltool,
 itstool,
 libarchive-dev (>= 3.0.0),
 libboost-dev (>= 1.49.0),
 libcurl4-gnutls-dev (>= 7.25.0),
 libfftw3-dev (>= 3.3.1),
 liblrdf0-dev (>= 0.4.0),
 libserd-dev (>= 0.18.2~),
 libsord-dev (>= 0.12.0~),
 libsuil-dev (>= 0.6.10~),
 liblilv-dev,
 libsratom-dev (>= 0.4.2~),
 libsigc++-2.0-dev (>= 2.2.10),
 uuid-dev,
 libdbus-1-dev,
 libxml2-dev (>= 2.5.7),
 libcwiid-dev [linux-any],
 libcairomm-1.0-dev (>= 1.10.0),
 libgtkmm-2.4-dev (>= 2.24.2),
 libpangomm-1.4-dev (>= 2.28.4),
 libhidapi-dev,
 libusb-1.0-0-dev,
 ladspa-sdk (>= 1.1-2),
 libasound2-dev (>= 0.9.4) [linux-any],
 libpulse-dev,
 libaubio-dev (>= 0.3.2),
 libfluidsynth-dev,
 libjack-dev,
 liblo-dev (>= 0.26~),
 libltc-dev,
 libqm-dsp-dev,
 librubberband-dev,
 libsamplerate0-dev (>= 0.1.8),
 libsndfile1-dev (>= 1.0.25),
 libtag1-dev,
 libwebsockets-dev (>= 2.0.0),
 lv2-dev (>= 1.2.0),
 vamp-plugin-sdk (>=2.1),
 python3-setuptools,
 python3-isodate,
 python3-rdflib,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://www.ardour.org/
Vcs-Git: https://salsa.debian.org/multimedia-team/ardour.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/ardour

Package: ardour
Architecture: any
Depends:
 ardour-data (>= ${source:Version}),
 ardour-data (<< ${source:Upstream-Version}.0~),
 ardour-lv2-plugins (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ardour-video-timeline,
Suggests:
 ${shlibs:Suggests},
Replaces:
 ardour3 (<< 1:3.5),
Breaks:
 ardour3 (<< 1:3.5),
Description: the digital audio workstation
 Ardour is a multichannel hard disk recorder (HDR) and digital audio
 workstation (DAW).  It can be used to control, record, edit and run
 complex audio setups.
 .
 Ardour supports pro-audio interfaces
 through the ALSA project, which provides high quality, well designed
 device drivers and API's for audio I/O under Linux. Any interface
 supported by ALSA can be used with Ardour. This includes the
 all-digital 26 channel RME Hammerfall, the Midiman Delta 1010 and many
 others.
 .
 Ardour has support for 24 bit samples
 using floating point internally, non-linear editing with unlimited undo,
 a user-configurable mixer, MTC master/slave capabilities, MIDI hardware
 control surface compatibility.
 .
 It supports MIDI
 Machine Control, and so can be controlled from any MMC controller and
 many modern digital mixers.
 .
 Ardour contains a
 powerful multitrack audio editor/arranger that is completely
 non-destructive and capable of all standard non-linear editing
 operations (insert, replace, delete, move, trim, select,
 cut/copy/paste). The editor has unlimited undo/redo capabilities and can
 save independent "versions" of a track or an entire piece
 .
 Ardour's editor supports the community-developed LADSPA
 plugin standard.  Arbitrary chains of plugins can be attached to any
 portion of a track.  Every mixer strip can have any number of inputs
 and outputs, not just mono, stereo or 5.1.  An N-way panner is
 included, with support for various panning models.  Pre- and post-fader
 sends exist, each with their own gain and pan controls.  Every mixer
 strip acts as its own bus, and thus the bus count in Ardour is
 unlimited.  You can submix any number of strips into another
 strip.
 .
 Ardour's channel capacity is limited only
 by the number on your audio interface and the ability of your disk
 subsystem to stream the data back and forth.
 .
 JACK
 (the JACK Audio Connection Kit) is used for all audio I/O, permitting
 data to be exchanged in perfect samplesync with other applications
 and/or hardware audio interfaces.
 .
 Ardour is
 sample rate and size neutral - any hardware formats from 8 to 32 bits,
 and rates from 8kHz to 192kHz.  Internal processing in 32/64 bit IEEE
 floating point format.
 .
 Further information can be
 found at <https://ardour.org/>.

Package: ardour-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: ardour
Breaks: ardour (<= 1:4.2~dfsg-1),
 ardour3 (<< 1:3.5),
Replaces: ardour (<= 1:4.2~dfsg-1),
 ardour3 (<< 1:3.5),
Description: digital audio workstation (data)
 Ardour is a multichannel hard disk recorder (HDR) and digital audio
 workstation (DAW).  It can be used to control, record, edit and run
 complex audio setups.
 .
 This package contains the architecture-independent data files.

Package: ardour-video-timeline
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
 xjadeo (>= 0.6.4),
 harvid (>= 0.7.0),
 ffmpeg (>= 7:1.2),
Description: digital audio workstation - video timeline
 Ardour is a multichannel hard disk recorder (HDR) and digital audio
 workstation (DAW).  It can be used to control, record, edit and run
 complex audio setups.
 .
 This is a dependency package to add video timeline capabilities to ardour.

Package: ardour-lv2-plugins
Architecture: any
Depends: ${misc:Depends}
Provides: lv2-plugin
Description: digital audio workstation - lv2 plugins set
 Ardour is providing a set of LV2 plugins:
  * a-comp.lv2
    - a compressor (mono & stereo versions)
  * a-exp.lv2
    - an expander (mono & stereo versions)
  * a-delay.lv2
    - a delay (mono version)
  * a-eq.lv2
    - an equaliser (mono version)
  * a-reverb.lv2
    - a reverberation (stereo version)
  * a-fluidsynth.lv2
    - a SF2 player (stereo version)
  * reasonablesynth.lv2
    - a reasonably good piano synth (stereo version)
 .
 This package allows these to be used by other hosts as well.

