#include "ardour/ardour.h"
namespace ARDOUR {
const char* const ardour_config_info = "\n\
Build documentation: False\n\
Debuggable build: True\n\
Export all symbols (backtrace): False\n\
Install prefix: /usr/local\n\
Strict compiler flags: []\n\
Internal Shared Libraries: True\n\
Use External Libraries: False\n\
Library exports hidden: True\n\
Free/Demo copy: False\n\
\n\
ALSA DBus Reservation: False\n\
Architecture flags: None\n\
ARM NEON support: False\n\
Aubio: True\n\
AudioUnits: False\n\
Build target: x86_64\n\
Canvas Test UI: False\n\
Beatbox test app: False\n\
CoreAudio: False\n\
CoreAudio 10.5 compat: False\n\
Debug RT allocations: False\n\
Debug Symbols: True\n\
Denormal exceptions: False\n\
Dr. Mingw: False\n\
FLAC: True\n\
FPU optimization: True\n\
FPU AVX/FMA support: True\n\
Freedesktop files: False\n\
Libjack linking: link\n\
Libjack metadata: True\n\
Lua Binding Doc: False\n\
Lua Commandline Tool: True\n\
LV2 UI embedding: True\n\
LV2 support: True\n\
LV2 extensions: True\n\
LXVST support: True\n\
Mac VST support: False\n\
NI-Maschine: False\n\
OGG: True\n\
Phone home: True\n\
Process thread timing: False\n\
Program name: Ardour\n\
Samplerate: True\n\
PT format: False\n\
PTW32 Semaphore: False\n\
Threaded WaveViews: True\n\
Translation: True\n\
Unit tests: False\n\
Use LLD linker: True\n\
VST3 support: True\n\
Windows VST support: False\n\
Wiimote support: True\n\
Windows key: Mod4><Super\n\
\n\
PortAudio Backend: False\n\
CoreAudio/Midi Backend: False\n\
ALSA Backend: True\n\
Dummy backend: True\n\
JACK Backend: True\n\
Pulseaudio Backend: True\n\
\n\
Buildstack: -system-\n\
Mac i386 Architecture: False\n\
Mac ppc Architecture: False\n\
Mac arm64 Architecture: False\n\
\n\
C compiler flags: ['-I/tmp/ardour', '-DHAVE_RF64_RIFF', '-DWAF_BUILD', '-g', '-std=c99', '-pedantic', '-Wshadow', '-Wall', '-Wcast-align', '-Wextra', '-Wwrite-strings', '-Wunsafe-loop-optimizations', '-Wlogical-op', '-fshow-column', '-DARCH_X86', '-mmmx', '-msse', '-mfpmath=sse', '-DUSE_XMMINTRIN', '-DBUILD_SSE_OPTIMIZATIONS', '-DLXVST_64BIT', '-Wall', '-Wpointer-arith', '-Wcast-qual', '-Wcast-align', '-Wno-unused-parameter', '-DBOOST_SYSTEM_NO_DEPRECATED', '-D_ISOC9X_SOURCE', '-D_LARGEFILE64_SOURCE', '-D_FILE_OFFSET_BITS=64', '-DPROGRAM_NAME=\"Ardour\"', '-DPROGRAM_VERSION=\"6\"', '-Wstrict-prototypes', '-Wmissing-prototypes', '-isystem', '/usr/include/gtk-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/gtk-2.0/include', '-isystem', '/usr/include/pango-1.0', '-isystem', '/usr/include/atk-1.0', '-isystem', '/usr/include/gdk-pixbuf-2.0', '-isystem', '/usr/include/libmount', '-isystem', '/usr/include/blkid', '-isystem', '/usr/include/fribidi', '-isystem', '/usr/include/cairo', '-isystem', '/usr/include/pixman-1', '-isystem', '/usr/include/harfbuzz', '-isystem', '/usr/include/glib-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/glib-2.0/include', '-isystem', '/usr/include/uuid', '-isystem', '/usr/include/freetype2', '-isystem', '/usr/include/libpng16', '-isystem', '/usr/include/gtkmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/gtkmm-2.4/include', '-isystem', '/usr/include/atkmm-1.6', '-isystem', '/usr/include/gtk-unix-print-2.0', '-isystem', '/usr/include/gtk-2.0', '-isystem', '/usr/include/gdkmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/gdkmm-2.4/include', '-isystem', '/usr/include/giomm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/giomm-2.4/include', '-isystem', '/usr/include/pangomm-1.4', '-isystem', '/usr/lib/x86_64-linux-gnu/pangomm-1.4/include', '-isystem', '/usr/include/glibmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/glibmm-2.4/include', '-isystem', '/usr/include/cairomm-1.0', '-isystem', '/usr/lib/x86_64-linux-gnu/cairomm-1.0/include', '-isystem', '/usr/include/sigc++-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/sigc++-2.0/include', '-isystem', '/usr/lib/x86_64-linux-gnu/gtk-2.0/include', '-isystem', '/usr/include/pango-1.0', '-isystem', '/usr/include/atk-1.0', '-isystem', '/usr/include/gdk-pixbuf-2.0', '-isystem', '/usr/include/libmount', '-isystem', '/usr/include/blkid', '-isystem', '/usr/include/fribidi', '-isystem', '/usr/include/cairo', '-isystem', '/usr/include/pixman-1', '-isystem', '/usr/include/harfbuzz', '-isystem', '/usr/include/glib-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/glib-2.0/include', '-isystem', '/usr/include/uuid', '-isystem', '/usr/include/freetype2', '-isystem', '/usr/include/libpng16']\n\
C++ compiler flags: ['-I/tmp/ardour', '-DHAVE_RF64_RIFF', '-DWAF_BUILD', '-g', '-ansi', '-Wnon-virtual-dtor', '-Woverloaded-virtual', '-fstrict-overflow', '-Wall', '-Wcast-align', '-Wextra', '-Wwrite-strings', '-Wunsafe-loop-optimizations', '-Wlogical-op', '-fshow-column', '-DARCH_X86', '-mmmx', '-msse', '-mfpmath=sse', '-DUSE_XMMINTRIN', '-DBUILD_SSE_OPTIMIZATIONS', '-DLXVST_64BIT', '-Wall', '-Wpointer-arith', '-Wcast-qual', '-Wcast-align', '-Wno-unused-parameter', '-DBOOST_SYSTEM_NO_DEPRECATED', '-D_ISOC9X_SOURCE', '-D_LARGEFILE64_SOURCE', '-D_FILE_OFFSET_BITS=64', '-DPROGRAM_NAME=\"Ardour\"', '-DPROGRAM_VERSION=\"6\"', '-std=c++11', '-DBOOST_NO_AUTO_PTR', '-DBOOST_BIND_GLOBAL_PLACEHOLDERS', '-Woverloaded-virtual', '-Wno-unused-local-typedefs', '-D__STDC_LIMIT_MACROS', '-D__STDC_FORMAT_MACROS', '-DCANVAS_COMPATIBILITY', '-DCANVAS_DEBUG', '-DBOOST_ERROR_CODE_HEADER_ONLY', '-isystem', '/usr/include/gtk-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/gtk-2.0/include', '-isystem', '/usr/include/pango-1.0', '-isystem', '/usr/include/atk-1.0', '-isystem', '/usr/include/gdk-pixbuf-2.0', '-isystem', '/usr/include/libmount', '-isystem', '/usr/include/blkid', '-isystem', '/usr/include/fribidi', '-isystem', '/usr/include/cairo', '-isystem', '/usr/include/pixman-1', '-isystem', '/usr/include/harfbuzz', '-isystem', '/usr/include/glib-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/glib-2.0/include', '-isystem', '/usr/include/uuid', '-isystem', '/usr/include/freetype2', '-isystem', '/usr/include/libpng16', '-isystem', '/usr/include/gtkmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/gtkmm-2.4/include', '-isystem', '/usr/include/atkmm-1.6', '-isystem', '/usr/include/gtk-unix-print-2.0', '-isystem', '/usr/include/gtk-2.0', '-isystem', '/usr/include/gdkmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/gdkmm-2.4/include', '-isystem', '/usr/include/giomm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/giomm-2.4/include', '-isystem', '/usr/include/pangomm-1.4', '-isystem', '/usr/lib/x86_64-linux-gnu/pangomm-1.4/include', '-isystem', '/usr/include/glibmm-2.4', '-isystem', '/usr/lib/x86_64-linux-gnu/glibmm-2.4/include', '-isystem', '/usr/include/cairomm-1.0', '-isystem', '/usr/lib/x86_64-linux-gnu/cairomm-1.0/include', '-isystem', '/usr/include/sigc++-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/sigc++-2.0/include', '-isystem', '/usr/lib/x86_64-linux-gnu/gtk-2.0/include', '-isystem', '/usr/include/pango-1.0', '-isystem', '/usr/include/atk-1.0', '-isystem', '/usr/include/gdk-pixbuf-2.0', '-isystem', '/usr/include/libmount', '-isystem', '/usr/include/blkid', '-isystem', '/usr/include/fribidi', '-isystem', '/usr/include/cairo', '-isystem', '/usr/include/pixman-1', '-isystem', '/usr/include/harfbuzz', '-isystem', '/usr/include/glib-2.0', '-isystem', '/usr/lib/x86_64-linux-gnu/glib-2.0/include', '-isystem', '/usr/include/uuid', '-isystem', '/usr/include/freetype2', '-isystem', '/usr/include/libpng16']\n\
Linker flags: ['-fuse-ld=lld', '']\n\
";
}
